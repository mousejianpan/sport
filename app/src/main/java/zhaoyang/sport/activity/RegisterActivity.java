package zhaoyang.sport.activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zhaoyang.sport.R;
import zhaoyang.sport.presenter.LoginRegisterIPresenter;

public class RegisterActivity extends AppCompatActivity implements IRegisterview {
    //https://www.apiopen.top/createUserKey?appId=com.chat.peakchaoeee&passwd=123456
    //register appkey
    //{"code":200,"msg":"成功!","data":{"appId":"zhaoyang.sport","appkey":"b59a057e50a1722f59087b699bdd0e3a"}}

    //https://www.jianshu.com/p/e6f072839282   api

    private LoginRegisterIPresenter loginRegisterPresenter;
    @BindView(R.id.pbregister)
    public ProgressBar pbRegister;
    @BindView(R.id.bt_register)
    public Button btRegister;
    @BindView(R.id.name_edit)
    public EditText tvRegister;
    @BindView(R.id.register_edit)
    public EditText etRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        loginRegisterPresenter = new LoginRegisterIPresenter(this);
    }

    @OnClick(R.id.bt_register)
    public void onClickRegister() {
        show();
        String user = tvRegister.getText().toString();
        String password = etRegister.getText().toString();
        loginRegisterPresenter.registerNet(user, password);
    }

    @Override
    public void show() {
        pbRegister.setVisibility(View.VISIBLE);
        btRegister.setVisibility(View.GONE);
    }

    @Override
    public void dismiss() {
        pbRegister.setVisibility(View.GONE);
        btRegister.setVisibility(View.VISIBLE);

    }

    @Override
    public void displayInfor(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
    }
}
