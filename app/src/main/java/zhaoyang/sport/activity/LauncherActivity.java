package zhaoyang.sport.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import zhaoyang.sport.R;
import zhaoyang.sport.adapter.AppsAdapter;

public class LauncherActivity extends Activity {
    private List<ResolveInfo> mApps;
    @BindView(R.id.apps_recyclerview)
    public RecyclerView rvApps;
    private RecyclerView.LayoutManager layoutManager;
    private AppsAdapter appsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadApps();
        setContentView(R.layout.activity_launcher);
        ButterKnife.bind(this);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        attributes.dimAmount = 0.6f;
        attributes.x = 0;
        attributes.y = 0;
        attributes.width = 800;
        attributes.height = 1024;
        getWindow().setAttributes(attributes);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initRecyclerView();
    }

    public void initRecyclerView() {
        Log.e("david", "size moive = " + mApps.size());
        layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
        ((StaggeredGridLayoutManager)layoutManager).setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        rvApps.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                ((StaggeredGridLayoutManager)layoutManager).invalidateSpanAssignments();
            }
        });
        appsAdapter = new AppsAdapter(this, mApps);
        rvApps.setLayoutManager(layoutManager);
        rvApps.setAdapter(appsAdapter);
        rvApps.addItemDecoration(new RecyclerView.ItemDecoration() {
            int space = 5;
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = space;
                outRect.right = space;
                outRect.bottom = space;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = space;
                //}
            }
        });
    }

    private void loadApps() {
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        mApps = getPackageManager().queryIntentActivities(mainIntent, 0);
    }
}
