package zhaoyang.sport.activity;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public interface IRegisterview {
    void show();
    void dismiss();
    void displayInfor(String message);
}
