package zhaoyang.sport.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import zhaoyang.sport.R;
import zhaoyang.sport.adapter.CinemaAdapter;
import zhaoyang.sport.adapter.ShareAdapter;
import zhaoyang.sport.presenter.SharePresenter;

public class ShareInfoActivity extends AppCompatActivity {

    @BindView(R.id.shareview)
    public RecyclerView rvShare;
    @BindView(R.id.shares_progress)
    public ProgressBar pbShares;
    RecyclerView.LayoutManager layoutManager;
    private ShareAdapter shareAdapter;
    private ArrayList<String> shareList;
    private SharePresenter sharePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_info);
        shareList = new ArrayList<>();
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        Intent intent = getIntent();
        sharePresenter = new SharePresenter(intent.getStringExtra("code"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareList = sharePresenter.getShareData();
        pbShares.setVisibility(View.GONE);
        rvShare.setVisibility(View.VISIBLE);
        initView();
    }

    public void initView() {
        layoutManager = new LinearLayoutManager(this);
        rvShare.setLayoutManager(layoutManager);
        shareAdapter = new ShareAdapter(this, shareList);
        rvShare.setAdapter(shareAdapter);
        rvShare.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = 5;
                outRect.right = 5;
                //outRect.bottom = 10;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = 10;
                //}
            }
        });
    }
}
