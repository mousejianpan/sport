package zhaoyang.sport.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.schedulers.Schedulers;
import zhaoyang.sport.Bean.Cinema;
import zhaoyang.sport.R;
import zhaoyang.sport.adapter.CinemaAdapter;
import zhaoyang.sport.presenter.CinemaPresenter;

public class CinemaActivity extends AppCompatActivity {
    private RecyclerView.LayoutManager layoutManager;
    private CinemaAdapter cinemaAdapter;
    private ArrayList<Cinema> cinemaArrayList;
    private CinemaPresenter cinemaPresenter;
    @BindView(R.id.cinema)
    public RecyclerView rv;
    @BindView(R.id.instruction)
    public TextView tvIns;
    @BindView(R.id.cinema_progress)
    public ProgressBar pbProgressbar;
    @BindView(R.id.moiveinfo_textview)
    public CardView cvMoiveInfo;
    private int moiveId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        EventBus.getDefault().register(this);
        cinemaArrayList = new ArrayList<>();
        cinemaPresenter = new CinemaPresenter(this);
        initRecycleView();
        Intent intent = getIntent();
        moiveId = intent.getIntExtra("moiveid", 0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        cinemaArrayList = cinemaPresenter.loadCinemaData(moiveId);
    }

    public void initRecycleView() {
        layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);
        cinemaAdapter = new CinemaAdapter(this, cinemaArrayList);
        rv.setAdapter(cinemaAdapter);
        rv.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = 5;
                outRect.right = 5;
                //outRect.bottom = 10;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = 20;
                //}
            }
        });
    }

    @Subscribe(priority = 1000)
    public void OnEvent(ArrayList<Cinema> cinemaArrayList) {
        if (cinemaArrayList.size() > 0) {
            if (cinemaArrayList.get(0) instanceof Cinema) {
                pbProgressbar.setVisibility(View.GONE);
                Log.e("david", "receive cinema" + Thread.currentThread().toString());
                cinemaAdapter.updateData(cinemaArrayList);
                cinemaAdapter.notifyDataSetChanged();
            }
        }
    }

    @Subscribe
    public void OnEventString(StringBuilder stringBuilder) {
        cvMoiveInfo.setVisibility(View.VISIBLE);
        Log.e("david", "receive cinema string" + Thread.currentThread().toString());
        updateDataString(stringBuilder);
    }

    public void updateDataString(StringBuilder stringBuilder) {
        tvIns.setText(String.format(getResources().getString(R.string.instruction), stringBuilder));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
