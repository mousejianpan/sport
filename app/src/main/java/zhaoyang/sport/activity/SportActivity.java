package zhaoyang.sport.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.shizhefei.view.indicator.FixedIndicatorView;
import com.shizhefei.view.indicator.IndicatorViewPager;
import com.shizhefei.view.indicator.transition.OnTransitionTextListener;
import com.shizhefei.view.viewpager.SViewPager;

import zhaoyang.sport.R;
import zhaoyang.sport.fragment.HappyFragment;
import zhaoyang.sport.fragment.SharesFragment;
import zhaoyang.sport.fragment.MoiveFragment;
import zhaoyang.sport.fragment.WeatherFragment;

public class SportActivity extends AppCompatActivity{
    private IndicatorViewPager indicatorViewPager;
    private View centerView;
    private FixedIndicatorView indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        initView();
    }

    public void initView() {
        SViewPager viewPager = (SViewPager) findViewById(R.id.tabmain_viewPager);
        indicator = (FixedIndicatorView) findViewById(R.id.tabmain_indicator);
        indicator.setOnTransitionListener(new OnTransitionTextListener().setColor(Color.RED, Color.GRAY));

        //add a view, put it in center
        centerView = getLayoutInflater().inflate(R.layout.ccenterview_layout, indicator, false);
        indicator.setCenterView(centerView);
        indicator.setPadding(0,3,0,0);
        centerView.setOnClickListener(onClickListener);

        indicatorViewPager = new IndicatorViewPager(indicator, viewPager);
        indicatorViewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        // prohibit srcoll change
        viewPager.setCanScroll(false);
        // it set keep off screen page number, it will dno't need load again
        viewPager.setOffscreenPageLimit(4);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == centerView) {
                //remove centerview
                //indicator.removeCenterView();
                //Toast.makeText(getApplicationContext(), "点击了CenterView", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SportActivity.this, LauncherActivity.class);
                startActivity(intent);
            }
        }
    };

    private class MyAdapter extends IndicatorViewPager.IndicatorFragmentPagerAdapter {
        private String[] tabNames = {getResources().getString(R.string.movie),
                getResources().getString(R.string.weather), getResources().getString(R.string.shares),
                getResources().getString(R.string.happy)};
        private int[] tabIcons = {R.mipmap.moive, R.mipmap.weather, R.mipmap.shares,
                R.mipmap.xiankan};
        private LayoutInflater inflater;

        public MyAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            inflater = LayoutInflater.from(getApplicationContext());
        }

        @Override
        public int getCount() {
            return tabNames.length;
        }

        @Override
        public View getViewForTab(int position, View convertView, ViewGroup container) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.tabtext_layout, container, false);
            }
            TextView textView = (TextView) convertView;
            textView.setText(tabNames[position]);
            textView.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[position], 0, 0);
            return textView;
        }

        @Override
        public Fragment getFragmentForPage(int position) {
            //TestFragment mainFragment = new TestFragment();
            //Bundle bundle = new Bundle();
            //bundle.putString(TestFragment.INTENT_STRING_TABNAME, tabNames[position]);
            //bundle.putInt(TestFragment.INTENT_INT_INDEX, position);
            //mainFragment.setArguments(bundle);
            switch(position) {
                case 0:
                    return new MoiveFragment();
                case 1:
                    return new WeatherFragment();
                case 2:
                    return new SharesFragment();
                default:
                    return new HappyFragment();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this,
                android.R.style.Theme_Material_Light_Dialog_Alert));
        AlertDialog alertDialog = builder.setTitle(getResources().getString(R.string.exit))
                .setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        System.exit(0);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                }).create();
        alertDialog.show();
    }
}
