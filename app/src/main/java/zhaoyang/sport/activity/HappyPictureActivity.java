package zhaoyang.sport.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;
import zhaoyang.sport.R;

public class HappyPictureActivity extends Activity {
    @BindView(R.id.happyPhotView)
    public PhotoView photoView;
    private StringBuilder urlString;
    private PhotoViewAttacher attacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_picture);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        urlString = new StringBuilder();
        urlString.append(intent.getStringExtra("url"));
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        attributes.dimAmount = 0.6f;
        attributes.x = 0;
        attributes.y = 0;
        getWindow().setAttributes(attributes);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initPhotoView();
    }

    public void initPhotoView() {
        attacher = new PhotoViewAttacher(photoView);
        Glide.with(this).load(urlString.toString()).placeholder(R.mipmap.default_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(photoView);
        attacher.setMinimumScale(0.5f);
        attacher.setMaximumScale(5f);
        attacher.update();
    }
}
