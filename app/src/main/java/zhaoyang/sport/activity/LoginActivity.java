package zhaoyang.sport.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import zhaoyang.sport.R;
import zhaoyang.sport.presenter.LoginRegisterIPresenter;

public class LoginActivity extends AppCompatActivity implements IRegisterview {
    private String TAG = "LoginActivity";
    @BindView(R.id.login_edit)
    public EditText nameEdit;
    @BindView(R.id.password_edit)
    public EditText passwordEdit;
    @BindView(R.id.pblogin)
    public ProgressBar pbLogin;
    @BindView(R.id.btlogin)
    public Button btLogin;
    //private StringBuilder netResult;
    private LoginRegisterIPresenter loginRegisterIPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        //netResult = new StringBuilder();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        loginRegisterIPresenter = new LoginRegisterIPresenter(this);
    }

    @OnClick(R.id.btlogin)
    public void clickLogin() {
        show();
        String user = nameEdit.getText().toString();
        String password = passwordEdit.getText().toString();
        loginRegisterIPresenter.loginNet(user, password);
    }

    @Override
    public void show() {
        pbLogin.setVisibility(View.VISIBLE);
        btLogin.setVisibility(View.GONE);
    }

    @Override
    public void dismiss() {
        pbLogin.setVisibility(View.GONE);
        btLogin.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayInfor(final String message) {
        //netResult.append(message);
        Log.e("david", "message" + message);
        //Log.e("david", "netResult" + netResult.toString());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                dismiss();
                String result = getResources().getString(R.string.success);
                Log.e("david", "result" + result);
                if (result.equals(message)) {
                    //netResult.setLength(0);
                    //netResult.delete(0, netResult.length());
                    Intent intent = new Intent(LoginActivity.this, SportActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    nameEdit.getText().clear();
                    passwordEdit.getText().clear();
                }
            }
        });

    }
}
