package zhaoyang.sport.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import zhaoyang.sport.R;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-8.
 */

public class ShareAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<String> shareList;

    public ShareAdapter(Context context, ArrayList<String> shareList) {
        this.context = context;
        this.shareList = shareList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.share_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        ButterKnife.bind(this, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (shareList.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            myViewHolder.tv.setText(shareList.get(position));
            Glide.with(context).load(shareList.get(position)).
                    placeholder(R.mipmap.default_error).into(myViewHolder.shareImage);
        }
    }

    @Override
    public int getItemCount() {
        Log.e("david", "share size");
        return shareList.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder != null) {
            //when holder recycle(reuse) , glide stop load image to it
            Glide.clear(((MyViewHolder)holder).shareImage);
        }
        super.onViewRecycled(holder);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sharecat)
        public TextView tv;
        @BindView(R.id.shareimage)
        public ImageView shareImage;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
