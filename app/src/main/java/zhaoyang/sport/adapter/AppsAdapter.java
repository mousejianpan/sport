package zhaoyang.sport.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import zhaoyang.sport.R;

/**
 * Created by zhaoyang on 2018/5/10.
 */

public class AppsAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<ResolveInfo> mApps;

    public AppsAdapter(Context context, List<ResolveInfo> mApps) {
        this.context = context;
        this.mApps = mApps;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.apps_item, null);
        MyViewHolder viewHolder = new MyViewHolder(view);
        ButterKnife.bind(this, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (mApps.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            ResolveInfo info = mApps.get(position);
            myViewHolder.ivApp.setImageDrawable(info.activityInfo.loadIcon(context.getPackageManager()));
            //Glide.with(context).load(info.activityInfo.loadIcon(context.getPackageManager())).into(myViewHolder.ivApp);
            myViewHolder.tvApp.setText(info.activityInfo.loadLabel(context.getPackageManager()));
            myViewHolder.ivApp.setTag(position);
            Log.e("david", "size moive = " + mApps.size() + info.activityInfo.loadLabel(context.getPackageManager()));
        }
    }

    @Override
    public int getItemCount() {
        return mApps.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //@BindView(R.id.app_imageview)
        public ImageView ivApp;
        //@BindView(R.id.app_textview)
        public TextView tvApp;

        private MyViewHolder(View itemView) {
            super(itemView);
            ivApp = (ImageView)itemView.findViewById(R.id.app_imageview);
            tvApp = (TextView)itemView.findViewById(R.id.app_textview);
            //ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("david", "555" + ivApp.getTag());
                    ResolveInfo info = mApps.get(Integer.parseInt(ivApp.getTag().toString()));
                    //app package name
                    String pkg = info.activityInfo.packageName;
                    //app main activity
                    String cls = info.activityInfo.name;
                    ComponentName componet = new ComponentName(pkg, cls);
                    Intent intent = new Intent();
                    intent.setComponent(componet);
                    context.startActivity(intent);
                }
            });
        }
    }
}
