package zhaoyang.sport.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import zhaoyang.sport.Bean.Cinema;
import zhaoyang.sport.R;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-7.
 */

public class CinemaAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<Cinema> cinemaList;

    public CinemaAdapter(Context context, ArrayList<Cinema> cinemaList) {
        this.context = context;
        this.cinemaList = cinemaList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.cinema_item, parent, false);
        RecyclerView.ViewHolder viewHolder = new CinemaAdapter.MyViewHolder(view);
        ButterKnife.bind(this, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.e("david", "size cine = " + cinemaList.size());
        if (cinemaList.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            myViewHolder.tv.get(0).setText(String.format(context.getResources().getString(R.string.cinemaname),
                    cinemaList.get(position).getName()));
            myViewHolder.tv.get(1).setText(String.format(context.getResources().getString(R.string.cinemaaddr),
                    cinemaList.get(position).getAddr()));
        }
    }

    @Override
    public int getItemCount() {
        Log.e("david", "getItemCount = " + cinemaList.size());
        return cinemaList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.cinema_name, R.id.cinema_addr})
        public List<TextView> tv;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(ArrayList<Cinema> cinemaList) {
        this.cinemaList = cinemaList;
        Log.e("david", "update data size = " + cinemaList.size());
    }
}
