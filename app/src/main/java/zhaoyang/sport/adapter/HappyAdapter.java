package zhaoyang.sport.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import cn.jzvd.JZVideoPlayerStandard;
import zhaoyang.sport.Bean.Happy;
import zhaoyang.sport.R;
import zhaoyang.sport.activity.HappyPictureActivity;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public class HappyAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<Happy> happyList;

    public HappyAdapter(Context context, ArrayList<Happy> happyList) {
        this.context = context;
        this.happyList = happyList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.happy_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        ButterKnife.bind(this, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (happyList.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            Happy happy = new Happy();
            happy = happyList.get(position);
            myViewHolder.tvHappy.setTag(position);
            int type = happy.getType();
            myViewHolder.linearList.get(0).setVisibility(View.GONE);
            myViewHolder.linearList.get(1).setVisibility(View.GONE);
            myViewHolder.linearList.get(2).setVisibility(View.GONE);
            switch (type) {
                case 41:
                    myViewHolder.linearList.get(0).setVisibility(View.VISIBLE);
                    myViewHolder.videoPlayerStandard.setUp(happy.getVideoUrl(),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL, happy.getText());
                    Glide.with(context).load(happy.getPictureUrl())
                            .diskCacheStrategy(DiskCacheStrategy.ALL).
                            into(myViewHolder.videoPlayerStandard.thumbImageView);
                    break;
                case 10:
                    myViewHolder.linearList.get(1).setVisibility(View.VISIBLE);
                    myViewHolder.tvImageText.setText(happy.getText());
                    Glide.with(context).load(happy.getPictureUrl())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.mipmap.default_error).into(myViewHolder.ivHapppy);
                    break;
                case 29:
                    myViewHolder.linearList.get(2).setVisibility(View.VISIBLE);
                    myViewHolder.tvHappy.setText(happy.getText());
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        Log.e("david", "share size");
        return happyList.size();
    }

    public void updateData(ArrayList<Happy> arrayList) {
        this.happyList = arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.video_info, R.id.picture_info, R.id.duanzi_info})
        public List<LinearLayout> linearList;
        @BindView(R.id.videoplayer)
        public JZVideoPlayerStandard videoPlayerStandard;
        @BindView(R.id.pic_image)
        public ImageView ivHapppy;
        @BindView(R.id.duanzi_textview)
        public TextView tvHappy;
        @BindView(R.id.happyPictureText)
        public TextView tvImageText;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = Integer.parseInt(tvHappy.getTag().toString());
                    if (happyList.get(position).getType() == 10) {
                        Intent intent = new Intent(context, HappyPictureActivity.class);
                        intent.putExtra("url", happyList.get(position).getPictureUrl());
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}