package zhaoyang.sport.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import zhaoyang.sport.Bean.Moive;
import zhaoyang.sport.R;
import zhaoyang.sport.activity.CinemaActivity;

/**
 * Created by zhaoyang on 2018/5/5.
 */

public class MoiveAdapter extends RecyclerView.Adapter {
    private Context context;
    private ArrayList<Moive> moiveList;

    public MoiveAdapter(Context context, ArrayList<Moive> moiveList) {
        this.context = context;
        this.moiveList = moiveList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return null;
        View view = LayoutInflater.from(context).inflate(R.layout.moive_item, parent, false);
        RecyclerView.ViewHolder viewHolder = new MyViewHolder(view);
        ButterKnife.bind(this, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Log.e("david", "size moive = " + moiveList.size() + position);
        if (moiveList.size() > 0) {
            MyViewHolder myViewHolder = (MyViewHolder)holder;
            myViewHolder.tv.get(0).setText(String.format(context.getResources().getString(R.string.moivename),
                    moiveList.get(position).getName()));
            myViewHolder.tv.get(1).setText(String.format(context.getResources().getString(R.string.duration),
                    moiveList.get(position).getStar()));
            myViewHolder.tv.get(2).setText(String.format(context.getResources().getString(R.string.star),
                    moiveList.get(position).getTime()));
            myViewHolder.tv.get(3).setText(String.format(context.getResources().getString(R.string.version),
                    moiveList.get(position).getVersion()));
            Log.e("david--", moiveList.get(position).getName().toString());
            if (moiveList.get(position).getPicture() != null) {
                Glide.with(context)
                        .load(Uri.parse(moiveList.get(position).getPicture()))
                        .into(myViewHolder.imageView);
            } else {
                myViewHolder.imageView.setImageResource(R.mipmap.ic_launcher);
            }
            myViewHolder.tv.get(0).setTag(position);
        }
    }

    @Override
    public int getItemCount() {
        return moiveList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.moive_image)
        public ImageView imageView;
        @BindViews({R.id.moive_name, R.id.moive_star, R.id.moive_durtation, R.id.moive_version})
        public List<TextView> tv;

        private MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("david", "555" + tv.get(0).getTag() + moiveList.get(Integer.parseInt(tv.get(0).getTag().toString())).getName());
                    Intent intent = new Intent(context, CinemaActivity.class);
                    intent.putExtra("moiveid", moiveList.get(Integer.parseInt(tv.get(0).getTag().toString())).getId());
                    context.startActivity(intent);
                }
            });
        }
    }

    public void updateData(ArrayList<Moive> moiveList) {
        this.moiveList = moiveList;
    }
}
