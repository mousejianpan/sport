package zhaoyang.sport.utils;

import android.util.Log;
import android.util.SparseArray;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;
import zhaoyang.sport.Bean.Moive;
import zhaoyang.sport.adapter.MoiveAdapter;
import zhaoyang.sport.fragment.MoiveFragment;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-4.
 */

public class MoiveNetUtils {
    private ArrayList<Moive> moiveArrayList;

    public MoiveNetUtils() {
        moiveArrayList = new ArrayList<>();
    }

    //http://m.maoyan.com/movie/list.json?type=hot&offset=0&limit=1000
    public ArrayList<Moive> getDataFormNet() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://m.maoyan.com/movie/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        MoiveInterface moiveInterface = retrofit.create(MoiveInterface.class);
        Observable<ResponseBody> observable = moiveInterface.getMoiveInfo("list.json?type=hot&offset=0&limit=1000");
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONObject json = jsonObject.getJSONObject("data");
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = json.getJSONArray("movies");
                    /*jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    downLyric(stringBuilder.toString());*/
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Moive moive = new Moive();
                        JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                        moive.setCatalog(tempJsonObject.getString("cat"));
                        moive.setCountry(tempJsonObject.getString("src"));
                        moive.setDirector(tempJsonObject.getString("dir"));
                        moive.setTime(tempJsonObject.getString("dur"));
                        moive.setName(tempJsonObject.getString("nm"));
                        moive.setId(tempJsonObject.getInt("id"));
                        moive.setStar(tempJsonObject.getString("star"));
                        moive.setVersion(tempJsonObject.getString("ver"));
                        moive.setPicture(tempJsonObject.getString("img"));
                        moiveArrayList.add(moive);
                        moive = null;
                        //Log.e("david", moive.getName());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "notify data change");
                        EventBus.getDefault().postSticky(moiveArrayList);
                    }
                });
        Log.e("david", "net return");
        return moiveArrayList;
    }

    public interface MoiveInterface {
        @GET
        Observable<ResponseBody> getMoiveInfo(@Url String s);
    }

    public interface NotifyDataChange{
        void notifyChange();
    }
}
