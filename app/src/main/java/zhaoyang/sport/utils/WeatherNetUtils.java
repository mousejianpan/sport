package zhaoyang.sport.utils;

import android.util.Log;
import android.util.Xml;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;
import zhaoyang.sport.Bean.Weather;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-8.
 */

public class WeatherNetUtils {
    private ArrayList<Weather> weatherArrayList;

    public WeatherNetUtils() {
        weatherArrayList = new ArrayList<>();
    }

    //https://www.apiopen.top/weatherApi?city=%E5%A4%A9%E6%B4%A5
    public ArrayList<Weather> getDataFormNet() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.apiopen.top/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        WeatherInterface weatherInterface = retrofit.create(WeatherInterface.class);
        Observable<ResponseBody> observable = weatherInterface.getWeatherInfo("weatherApi?city=%E5%A4%A9%E6%B4%A5");
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONObject json = jsonObject.getJSONObject("data");
                    StringBuilder city = new StringBuilder();
                    city.append(json.getString("city"));
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = json.getJSONArray("forecast");
                    /*jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    downLyric(stringBuilder.toString());*///\u003c![CDATA[\u003c3级]]\u003e
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Weather weather = new Weather();
                        JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                        weather.setDate(tempJsonObject.getString("date"));
                        weather.setType(tempJsonObject.getString("type"));
                        weather.setWind(tempJsonObject.getString("fengxiang"));
                        //String test = tempJsonObject.getString("fengli").replace("\\", "");
                        String test = tempJsonObject.getString("fengli").replace("<", "");
                        String temp = test.substring(test.lastIndexOf('[') + 1, test.indexOf(']'));
                        Log.e("david", temp);
                        /*XmlPullParser parser = Xml.newPullParser();
                        parser.setInput(new StringReader(test));
                        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
                            Log.e("david", parser.getEventType() + "");
                            if (parser.getEventType() == XmlPullParser.START_TAG) {
                                String name = parser.getName();
                                Log.e("david", parser.getText());
                                parser.next();
                            }
                            parser.next();
                        }*/

                        weather.setGrade(temp);
                        weather.setHigh(tempJsonObject.getString("high"));
                        weather.setLow(tempJsonObject.getString("low"));
                        weather.setCity(city.toString());
                        Log.e("david", "weather = " + weather.toString());

                        weatherArrayList.add(weather);
                        weather = null;
                        //Log.e("david", moive.getName());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "notify data change");
                        EventBus.getDefault().postSticky(weatherArrayList);
                    }
                });
        Log.e("david", "net return");
        return weatherArrayList;
    }

    public interface WeatherInterface {
        @GET
        Observable<ResponseBody> getWeatherInfo(@Url String s);
    }

    public interface NotifyDataChange{
        void notifyChange();
    }
}
