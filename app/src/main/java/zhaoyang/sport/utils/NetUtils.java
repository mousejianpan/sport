package zhaoyang.sport.utils;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-3.
 */

public class NetUtils {
    public void goNet() {
        //http://geci.me/api/lyric/%E4%B8%BA%E7%88%B1%E7%97%B4%E7%8B%82
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://geci.me/api/lyric/")
                //.client(getOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        Lyric lyric = retrofit.create(Lyric.class);
        //Call<ResponseBody> call = lyric.getLyric("Sailing");
        Observable<ResponseBody> observable = lyric.getLyric("值得");
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("result");
                    jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    downLyric(stringBuilder.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {

                    }
                });
        /*call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("david = ", response.code() + response.message() + response.headers() + "///" + response.body().toString());

                if (response.body() == null || response.body().contentLength() == 0) {
                    Log.e("david", "body is null");
                    return;
                }
                ResponseBody responseBody = response.body();
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONArray jsonArray = new JSONArray();
                    jsonArray = jsonObject.getJSONArray("result");
                    jsonObject = jsonArray.getJSONObject(0);
                    final StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(jsonObject.getString("lrc"));
                    Log.e("david", "lyric  = " + stringBuilder);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            downLyric(stringBuilder.toString());
                        }
                    }).start();

                } catch (JSONException e) {
                    e.printStackTrace();
                }



                responseBody.close();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("david", "failure" + t.toString());

            }
        });*/
    }

    public void downLyric(String stringBuilder) {
        try {
            StringBuilder string = new StringBuilder();
            String s = null;
            URL url = new URL(stringBuilder);
            InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((s = bufferedReader.readLine()) != null) {
                string.append(s);
            }
            Log.e("david" , "lyric " + string);
            inputStreamReader.close();
            bufferedReader.close();
            File file = new File("/storage/emulated/0/lrctest");
            if (!file.exists()) {
                file.mkdirs();
            }
            StringBuilder fileName = new StringBuilder();
            fileName.append(file.getParent() + File.separator + file.getName() + ".lyc");
            Log.e("david", fileName.toString());
            File lyricFile = new File(fileName.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(lyricFile));
            outputStreamWriter.write(string.toString());
            outputStreamWriter.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private OkHttpClient getOkHttpClient() {
        //日志显示级别
        HttpLoggingInterceptor.Level level= HttpLoggingInterceptor.Level.BODY;
        //新建log拦截器
        HttpLoggingInterceptor loggingInterceptor=new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("david","OkHttp====Message:" + message);
            }
        });
        loggingInterceptor.setLevel(level);
        //定制OkHttp
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient
                .Builder();
        //OkHttp进行添加拦截器loggingInterceptor
        httpClientBuilder.addInterceptor(loggingInterceptor);
        return httpClientBuilder.build();
    }

    public interface Lyric {
        @GET
        Observable<ResponseBody> getLyric(@Url String s);
    }
}
