package zhaoyang.sport.utils;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-8.
 */

public class SharesNetUtils {
    private String code;

    public SharesNetUtils(String code) {
        this.code = code;
    }

    public ArrayList<String> getData() {
        Log.e("david", "code =" + code);
        ArrayList<String> share = new ArrayList<>();
        /*分时线的查询：
        http://image.sinajs.cn/newchart/min/n/sh000001.gif
        日K线查询：
        http://image.sinajs.cn/newchart/daily/n/sh000001.gif
        周K线查询：
        http://image.sinajs.cn/newchart/weekly/n/sh000001.gif
        月K线查询：
        http://image.sinajs.cn/newchart/monthly/n/sh000001.gif*/
        StringBuilder info = new StringBuilder();
        info.append("http://image.sinajs.cn/newchart/min/n/sh");
        info.append(code);
        info.append(".gif");
        share.add(info.toString());
        Log.e("david", "code =" + info);
        StringBuilder info1 = new StringBuilder();
        info1.append("http://image.sinajs.cn/newchart/daily/n/sh");
        info1.append(code);
        info1.append(".gif");
        share.add(info1.toString());
        StringBuilder info2 = new StringBuilder();
        info2.append("http://image.sinajs.cn/newchart/weekly/n/sh");
        info2.append(code);
        info2.append(".gif");
        share.add(info2.toString());
        StringBuilder info3 = new StringBuilder();
        info3.append("http://image.sinajs.cn/newchart/monthly/n/sh");
        info3.append(code);
        info3.append(".gif");
        share.add(info3.toString());
        return share;
    }
}
