package zhaoyang.sport.utils;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;
import zhaoyang.sport.Bean.Cinema;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-7.
 */

public class CinemaNetUtils {
    private ArrayList<Cinema> cinemaArrayList;
    private int id;
    private StringBuilder string = new StringBuilder();

    public CinemaNetUtils() {
        cinemaArrayList = new ArrayList<>();
    }

    //http://m.maoyan.com/cinemas.json
    public ArrayList<Cinema> getDataFormNet(int moiveId) {
        Retrofit retrofitname = new Retrofit.Builder()
                .baseUrl("http://m.maoyan.com/movie/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        CinemaNetUtils.CinemaInterface cinemaInterfacename = retrofitname.create(CinemaNetUtils.CinemaInterface.class);
        Observable<ResponseBody> observablename = cinemaInterfacename.getCinemaInfo(moiveId + ".json");
        observablename.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONObject json = jsonObject.getJSONObject("data");
                    JSONObject object = json.getJSONObject("MovieDetailModel");
                    string.append(object.getString("dra"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "update ins" + string);
                        EventBus.getDefault().post(string);
                    }
                });
        Log.e("david", "net return");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://m.maoyan.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        CinemaNetUtils.CinemaInterface cinemaInterface = retrofit.create(CinemaNetUtils.CinemaInterface.class);
        Observable<ResponseBody> observable = cinemaInterface.getCinemaInfo("cinemas.json");
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONObject json = jsonObject.getJSONObject("data");
                    Iterator<String> it = json.keys();
                    while(it.hasNext()){
                        String key = it.next();
                        String value = json.getString(key);
                        System.out.println("key: "+key+",value:"+value);
                        JSONArray jsonArray = json.getJSONArray(key);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            Cinema cinema = new Cinema();
                            JSONObject tempJsonObject = jsonArray.getJSONObject(i);
                            cinema.setName(tempJsonObject.getString("nm"));
                            cinema.setAddr(tempJsonObject.getString("addr"));
                            cinemaArrayList.add(cinema);
                            cinema = null;
                            //Log.e("david", moive.getName());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "notify data change");
                        EventBus.getDefault().post(cinemaArrayList);
                    }
                });
        Log.e("david", "net return");
        return cinemaArrayList;
    }

    public interface CinemaInterface {
        @GET
        Observable<ResponseBody> getCinemaInfo(@Url String s);
    }

    public interface NotifyDataChange{
        void notifyChange();
    }
}
