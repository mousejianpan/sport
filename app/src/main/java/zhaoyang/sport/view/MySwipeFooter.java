package zhaoyang.sport.view;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.aspsine.swipetoloadlayout.SwipeLoadMoreTrigger;
import com.aspsine.swipetoloadlayout.SwipeTrigger;

/**
 * Created by zhaoyang on 2018/5/12.
 */

public class MySwipeFooter extends View implements SwipeLoadMoreTrigger, SwipeTrigger {
    public MySwipeFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MySwipeFooter(Context context) {
        super(context);
    }

    private final static int DEFAULT_MAX_RADIUS = 16;
    private final static int DEFAULT_MIN_RADIUS = 5;
    private final static int DEFAULT_DISTANCE = 35;

    private final static int DEFAULT_ONE_BALL_COLOR = Color
            .parseColor("#40df73");
    private final static int DEFAULT_TWO_BALL_COLOR = Color
            .parseColor("#ffdf3e");
    private final static int DEFAULT_THREE_BALL_COLOR = Color
            .parseColor("#ff733e");

    private final static int DEFAULT_ANIMATOR_DURATION = 1400;

    private Paint mOnePaint;
    private Paint mTwoPaint;
    private Paint mThreePaint;

    private float maxRadius = DEFAULT_MAX_RADIUS;
    private float minRadius = DEFAULT_MIN_RADIUS;

    private int distance = DEFAULT_DISTANCE;

    private long duration = DEFAULT_ANIMATOR_DURATION;

    private MySwipeFooter.Ball mOneBall;
    private MySwipeFooter.Ball mTwoBall;
    private MySwipeFooter.Ball mThreeBall;

    private float mCenterX;
    private float mCenterY;

    private AnimatorSet animatorSet;

    private void init(Context context) {

        mOneBall = new Ball();
        mTwoBall = new Ball();
        mThreeBall = new Ball();

        mOneBall.setColor(DEFAULT_ONE_BALL_COLOR);
        mTwoBall.setColor(DEFAULT_TWO_BALL_COLOR);
        mThreeBall.setColor(DEFAULT_THREE_BALL_COLOR);

        mOnePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mOnePaint.setColor(DEFAULT_ONE_BALL_COLOR);
        mTwoPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTwoPaint.setColor(DEFAULT_TWO_BALL_COLOR);
        mThreePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mThreePaint.setColor(DEFAULT_THREE_BALL_COLOR);

        configAnimator();

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mCenterX = w / 2;
        mCenterY = h / 2;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mCenterX = getWidth() / 2;
        mCenterY = getHeight() / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mOneBall.getRadius() >= mTwoBall.getRadius()) {
            if (mThreeBall.getRadius() >= mOneBall.getRadius()) {
                canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                        mTwoBall.getRadius(), mTwoPaint);
                canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                        mOneBall.getRadius(), mOnePaint);
                canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                        mThreeBall.getRadius(), mThreePaint);
            } else {
                if (mTwoBall.getRadius() <= mThreeBall.getRadius()) {
                    canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                            mTwoBall.getRadius(), mTwoPaint);
                    canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                            mThreeBall.getRadius(), mThreePaint);
                    canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                            mOneBall.getRadius(), mOnePaint);
                } else {
                    canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                            mThreeBall.getRadius(), mThreePaint);
                    canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                            mTwoBall.getRadius(), mTwoPaint);
                    canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                            mOneBall.getRadius(), mOnePaint);

                }
            }
        } else {
            if (mThreeBall.getRadius() >= mTwoBall.getRadius()) {
                canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                        mOneBall.getRadius(), mOnePaint);
                canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                        mTwoBall.getRadius(), mTwoPaint);
                canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                        mThreeBall.getRadius(), mThreePaint);
            } else {
                if (mOneBall.getRadius() <= mThreeBall.getRadius()) {
                    canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                            mOneBall.getRadius(), mOnePaint);
                    canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                            mThreeBall.getRadius(), mThreePaint);
                    canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                            mTwoBall.getRadius(), mTwoPaint);
                } else {
                    canvas.drawCircle(mCenterX, mThreeBall.getCenterY(),
                            mThreeBall.getRadius(), mThreePaint);
                    canvas.drawCircle(mCenterX, mOneBall.getCenterY(),
                            mOneBall.getRadius(), mOnePaint);
                    canvas.drawCircle(mCenterX, mTwoBall.getCenterY(),
                            mTwoBall.getRadius(), mTwoPaint);

                }
            }
        }

    }

    private void configAnimator() {
        float centerRadius = (maxRadius + minRadius) * 0.5f;

        ObjectAnimator oneScaleAnimator = ObjectAnimator.ofFloat(mOneBall,
                "radius", centerRadius, maxRadius, centerRadius, minRadius,
                centerRadius);
        oneScaleAnimator.setRepeatCount(ValueAnimator.INFINITE);
        ValueAnimator oneCenterAnimator = ValueAnimator
                .ofFloat(-1, 0, 1, 0, -1);
        oneCenterAnimator.setRepeatCount(ValueAnimator.INFINITE);
        oneCenterAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        float y = mCenterY + (distance) * value;
                        mOneBall.setCenterY(y);
                        invalidate();
                    }
                });
        ValueAnimator oneAlphaAnimator = ValueAnimator.ofFloat(0.8f, 1, 0.8f,
                0, 0.8f);
        oneAlphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
        oneAlphaAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        int alpha = (int) (255 * value);
                        mOnePaint.setAlpha(alpha);
                    }
                });

        ObjectAnimator twoScaleAnimator = ObjectAnimator.ofFloat(mTwoBall,
                "radius", maxRadius, centerRadius, minRadius, centerRadius,
                maxRadius);
        twoScaleAnimator.setRepeatCount(ValueAnimator.INFINITE);

        ValueAnimator twoCenterAnimator = ValueAnimator.ofFloat(0, 1, 0, -1, 0);
        twoCenterAnimator.setRepeatCount(ValueAnimator.INFINITE);
        twoCenterAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        float y = mCenterY + (distance) * value;
                        mTwoBall.setCenterY(y);
                    }
                });
        ValueAnimator twoAlphaAnimator = ValueAnimator.ofFloat(1, 0.8f, 0,
                0.8f, 1);
        twoAlphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
        twoAlphaAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        int alpha = (int) (255 * value);
                        mTwoPaint.setAlpha(alpha);
                    }
                });

        ObjectAnimator threeScaleAnimator = ObjectAnimator.ofFloat(mThreeBall,
                "radius", centerRadius, minRadius, centerRadius, maxRadius,
                centerRadius);
        threeScaleAnimator.setRepeatCount(ValueAnimator.INFINITE);

        ValueAnimator threeCenterAnimator = ValueAnimator.ofFloat(1, 0, -1, 0,
                1);
        threeCenterAnimator.setRepeatCount(ValueAnimator.INFINITE);
        threeCenterAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        float y = mCenterY + (distance) * value;
                        mThreeBall.setCenterY(y);
                    }
                });
        ValueAnimator threeAlphaAnimator = ValueAnimator.ofFloat(0.8f, 0, 0.8f,
                1, 0.8f);
        threeAlphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
        threeAlphaAnimator
                .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float value = (Float) animation.getAnimatedValue();
                        int alpha = (int) (255 * value);
                        mThreePaint.setAlpha(alpha);
                    }
                });

        animatorSet = new AnimatorSet();
        animatorSet.playTogether(oneScaleAnimator, oneCenterAnimator,
                twoScaleAnimator, twoCenterAnimator, threeScaleAnimator,
                threeCenterAnimator);
        animatorSet.setDuration(DEFAULT_ANIMATOR_DURATION);
        animatorSet.setInterpolator(new LinearInterpolator());
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onPrepare() {

    }

    @Override
    public void onSwipe(int i, boolean b) {

    }

    @Override
    public void onRelease() {

    }

    @Override
    public void complete() {

    }

    @Override
    public void onReset() {

    }

    public class Ball {
        private float radius;
        private float centerX;
        private int color;
        private float centerY;

        public float getCenterY() {
            return centerY;
        }

        public void setCenterY(float centerY) {
            this.centerY = centerY;
        }

        public float getRadius() {
            return radius;
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public float getCenterX() {
            return centerX;
        }

        public void setCenterX(float centerX) {
            this.centerX = centerX;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }
    }

    @Override
    public void setVisibility(int v) {
        if (getVisibility() != v) {
            super.setVisibility(v);
            if (v == GONE || v == INVISIBLE) {
                stopAnimator();
            } else {
                startAnimator();
            }
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int v) {
        super.onVisibilityChanged(changedView, v);
        if (v == GONE || v == INVISIBLE) {
            stopAnimator();
        } else {
            startAnimator();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        startAnimator();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        stopAnimator();
    }

    public void setOneBallColor(int color) {
        mOneBall.setColor(color);
    }

    public void setmTwoBallColor(int color) {
        mTwoBall.setColor(color);
    }

    public void setMaxRadius(float maxRadius) {
        this.maxRadius = maxRadius;
        configAnimator();
    }

    public void setMinRadius(float minRadius) {
        this.minRadius = minRadius;
        configAnimator();
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setDuration(long duration) {
        this.duration = duration;
        if (animatorSet != null) {
            animatorSet.setDuration(duration);
        }
    }

    public void startAnimator() {
        if (getVisibility() != VISIBLE)
            return;

        if (animatorSet.isRunning())
            return;

        if (animatorSet != null) {
            animatorSet.start();
        }
    }

    public void stopAnimator() {
        if (animatorSet != null) {
            animatorSet.end();
        }
    }

}
