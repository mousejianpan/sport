package zhaoyang.sport.presenter;

import zhaoyang.sport.utils.NetUtils;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-3.
 */

public class HomeIPresenterImp implements IPresenter {
    private NetUtils netUtils;

    public HomeIPresenterImp() {
        netUtils = new NetUtils();
    }

    @Override
    public void loadData() {
        netUtils.goNet();
    }
}
