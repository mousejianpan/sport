package zhaoyang.sport.presenter;

import android.content.Context;

import java.util.ArrayList;

import zhaoyang.sport.Bean.Moive;
import zhaoyang.sport.utils.CinemaNetUtils;
import zhaoyang.sport.utils.MoiveNetUtils;

/**
 * Created by zhaoyang on 2018/5/5.
 */

public class MoivePresenter {
    private MoiveNetUtils moiveNetUtils;

    public MoivePresenter(Context cont) {
        this.moiveNetUtils = new MoiveNetUtils();
    }
    public ArrayList<Moive> loadMoiveData() {
        return moiveNetUtils.getDataFormNet();
    }

}
