package zhaoyang.sport.presenter;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-3.
 */

public interface IPresenter {
    void loadData();
}
