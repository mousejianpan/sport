package zhaoyang.sport.presenter;

import android.content.Context;

import java.util.ArrayList;

import zhaoyang.sport.Bean.Weather;
import zhaoyang.sport.utils.WeatherNetUtils;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-8.
 */

public class WeatherPresenter {
    private WeatherNetUtils weatherNetUtils;

    public WeatherPresenter(Context cont) {
        this.weatherNetUtils = new WeatherNetUtils();
    }
    public ArrayList<Weather> loadWheatherData() {
        return weatherNetUtils.getDataFormNet();
    }
}
