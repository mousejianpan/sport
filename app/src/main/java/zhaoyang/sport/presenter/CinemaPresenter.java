package zhaoyang.sport.presenter;

import android.content.Context;

import java.util.ArrayList;

import zhaoyang.sport.Bean.Cinema;
import zhaoyang.sport.utils.CinemaNetUtils;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-7.
 */

public class CinemaPresenter {
    private CinemaNetUtils cinemaNetUtils;

    public CinemaPresenter(Context cont) {
        this.cinemaNetUtils = new CinemaNetUtils();
    }
    public ArrayList<Cinema> loadCinemaData(int moiveId) {
        return cinemaNetUtils.getDataFormNet(moiveId);
    }
}
