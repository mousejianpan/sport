package zhaoyang.sport.presenter;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import zhaoyang.sport.activity.IRegisterview;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public class LoginRegisterIPresenter implements IPresenter {
    private IRegisterview registerview;
    private StringBuilder message;
    public LoginRegisterIPresenter(IRegisterview registerview) {
        this.registerview = registerview;
    }

    @Override
    public void loadData() {

    }

    public void registerNet(String user, String password) {
        message = new StringBuilder();
        //https://www.apiopen.top/createUser?key=00d91e8e0cca2b76f515926a36db68f5&phone=135943478&passwd=1234
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.apiopen.top/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        RegisterInterface registerInterface = retrofit.create(RegisterInterface.class);
        Observable<ResponseBody> observable = registerInterface.getInfo("b59a057e50a1722f59087b699bdd0e3a", user, password);
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    message.append(jsonObject.getString("msg"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "return" + message.toString());
                        registerview.displayInfor(message.toString());
                    }
                });
    }

    public interface RegisterInterface {
        //@GET("createUser?key=00d91e8e0cca2b76f515926a36db68f5&phone={phone}&passwd={password}")
        @GET("createUser")
        Observable<ResponseBody> getInfo(@Query("key") String key, @Query("phone") String phone, @Query("passwd") String password);

        //https://www.apiopen.top/login?key=00d91e8e0cca2b76f515926a36db68f5&phone=135943478&passwd=123
    }

    public void loginNet(String user, String password) {
        message = new StringBuilder();
        //https://www.apiopen.top/createUser?key=00d91e8e0cca2b76f515926a36db68f5&phone=135943478&passwd=1234
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.apiopen.top/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        LoginInterface loginInterface = retrofit.create(LoginInterface.class);
        Observable<ResponseBody> observable = loginInterface.getInfo("b59a057e50a1722f59087b699bdd0e3a", user, password);
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    message.append(jsonObject.getString("msg"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        Log.e("david", "return" + message.toString());
                        registerview.displayInfor(message.toString());
                    }
                });
    }

    public interface LoginInterface {
        //https://www.apiopen.top/login?key=00d91e8e0cca2b76f515926a36db68f5&phone=135943478&passwd=123
        @GET("login")
        Observable<ResponseBody> getInfo(@Query("key") String key, @Query("phone") String phone, @Query("passwd") String password);
    }

}
