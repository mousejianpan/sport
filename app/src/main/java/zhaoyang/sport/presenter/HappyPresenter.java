package zhaoyang.sport.presenter;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;
import zhaoyang.sport.Bean.Happy;
import zhaoyang.sport.fragment.IHappyView;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public class HappyPresenter {
    private IHappyView happyrview;
    private ArrayList<Happy> arrayList;
    public HappyPresenter(IHappyView happyrview) {
        this.happyrview = happyrview;
        arrayList = new ArrayList<>();
    }

    public void loadHappyNetData(int type, int page) {
        //https://www.apiopen.top/satinApi?type=1&page=1
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.apiopen.top/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        HappyInterface happyInterface = retrofit.create(HappyInterface.class);
        Observable<ResponseBody> observable = happyInterface.getInfo(type, page);
        observable.doOnNext(new Consumer<ResponseBody>() {
            @Override
            public void accept(ResponseBody responseBody) throws Exception {
                StringBuilder builder = new StringBuilder();
                try {
                    builder.append(responseBody.string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("david", "content = " + builder);
                try {
                    JSONObject jsonObject = new JSONObject(builder.toString());
                    JSONArray json = jsonObject.getJSONArray("data");
                    for (int i = 0; i < json.length(); i++) {
                        Happy happy = new Happy();
                        JSONObject temp = json.getJSONObject(i);
                        happy.setType(temp.getInt("type"));
                        happy.setText(temp.getString("text"));
                        happy.setVideoUrl(temp.getString("videouri"));
                        happy.setPictureUrl(temp.getString("image0"));
                        arrayList.add(happy);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseBody.close();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ResponseBody>() {
                    @Override
                    public void accept(ResponseBody responseBody) throws Exception {
                        happyrview.updateHappyData(arrayList);
                    }
                });
    }

    public interface HappyInterface {
        //https://www.apiopen.top/satinApi?type=1&page=1
        @GET("satinApi")
        Observable<ResponseBody> getInfo(@Query("type") int type, @Query("page") int page);
    }
}
