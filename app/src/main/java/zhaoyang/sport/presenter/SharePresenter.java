package zhaoyang.sport.presenter;

import java.util.ArrayList;

import zhaoyang.sport.utils.SharesNetUtils;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-8.
 */

public class SharePresenter {
    private SharesNetUtils sharesNetUtils;

    public SharePresenter(String code) {
        this.sharesNetUtils = new SharesNetUtils(code);
    }

    public ArrayList<String> getShareData() {
        return sharesNetUtils.getData();
    }
}
