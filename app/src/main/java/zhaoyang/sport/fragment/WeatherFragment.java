package zhaoyang.sport.fragment;

import android.content.Context;
import android.graphics.drawable.RippleDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.internal.operators.flowable.FlowableOnErrorReturn;
import zhaoyang.sport.Bean.Weather;
import zhaoyang.sport.R;
import zhaoyang.sport.presenter.WeatherPresenter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WeatherFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WeatherFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private WeatherPresenter weatherPresenter;
    private ArrayList<Weather> weatherArrayList;
    private Unbinder unbinder;
    @BindViews({R.id.date, R.id.type, R.id.wind, R.id.grade, R.id.high, R.id.low})
    List<TextView> tvFirst;
    @BindViews({R.id.onedate, R.id.onet, R.id.onew})
    List<TextView> tvFirstTab;
    @BindViews({R.id.secdate, R.id.sect, R.id.secw})
    List<TextView> tvSecondTab;
    @BindViews({R.id.thirddate, R.id.thirdt, R.id.thirdw})
    List<TextView> tvThirdTab;
    @BindViews({R.id.fourdate, R.id.fourt, R.id.fourw})
    List<TextView> tvFourTab;
    @BindViews({R.id.fivedate, R.id.fivet, R.id.fivew})
    List<TextView> tvFiveTab;
    @BindView(R.id.mycity)
    public CardView cvCity;
    @BindView(R.id.week)
    public CardView cvWeek;
    @BindView(R.id.wheather_progress)
    public ProgressBar pbWeather;

    public WeatherFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WeatherFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WeatherFragment newInstance(String param1, String param2) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        weatherArrayList = new ArrayList<>();
        weatherPresenter = new WeatherPresenter(getActivity());
        weatherPresenter.loadWheatherData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ArrayList<Weather> weatherArrayList) {
        Log.e("david", "receive weather");
        if (weatherArrayList.size() > 0) {
            if (weatherArrayList.get(0) instanceof Weather) {
                pbWeather.setVisibility(View.GONE);
                cvCity.setVisibility(View.VISIBLE);
                cvWeek.setVisibility(View.VISIBLE);
                this.weatherArrayList = weatherArrayList;
                initView();
            }
        }
    }

    public void initView() {
        Log.e("david", "city = " + weatherArrayList.get(0).getCity());
        for (int i = 0; i < 5; i++) {
            Weather weather = weatherArrayList.get(i);
            switch (i) {
                case 0:
                    tvFirstTab.get(0).setText(weather.getDate());
                    tvFirstTab.get(1).setText(String.format(getResources().getString(R.string.tempreture),
                            weather.getHigh(), weather.getLow(), weather.getType()));
                    tvFirstTab.get(2).setText(String.format(getResources().getString(R.string.wind),
                            weather.getWind(), weather.getGrade()));
                    tvFirst.get(0).setText(String.format(getResources().getString(R.string.citydate),
                            weather.getCity(), "  ", weatherArrayList.get(0).getDate()));
                    tvFirst.get(1).setText(weather.getType());
                    tvFirst.get(2).setText(weather.getWind());
                    tvFirst.get(3).setText(weather.getGrade());
                    tvFirst.get(4).setText(weather.getHigh());
                    tvFirst.get(5).setText(weather.getLow());
                    break;
                case 1:
                    tvSecondTab.get(0).setText(weather.getDate());
                    tvSecondTab.get(1).setText(String.format(getResources().getString(R.string.tempreture),
                            weather.getHigh(), weather.getLow(), weather.getType()));
                    tvSecondTab.get(2).setText(String.format(getResources().getString(R.string.wind),
                            weather.getWind(), weather.getGrade()));
                    break;
                case 2:
                    tvThirdTab.get(0).setText(weather.getDate());
                    tvThirdTab.get(1).setText(String.format(getResources().getString(R.string.tempreture),
                            weather.getHigh(), weather.getLow(), weather.getType()));
                    tvThirdTab.get(2).setText(String.format(getResources().getString(R.string.wind),
                            weather.getWind(), weather.getGrade()));
                    break;
                case 3:
                    tvFourTab.get(0).setText(weather.getDate());
                    tvFourTab.get(1).setText(String.format(getResources().getString(R.string.tempreture),
                            weather.getHigh(), weather.getLow(), weather.getType()));
                    tvFourTab.get(2).setText(String.format(getResources().getString(R.string.wind),
                            weather.getWind(), weather.getGrade()));
                    break;
                case 4:
                    tvFiveTab.get(0).setText(weather.getDate());
                    tvFiveTab.get(1).setText(String.format(getResources().getString(R.string.tempreture),
                            weather.getHigh(), weather.getLow(), weather.getType()));
                    tvFiveTab.get(2).setText(String.format(getResources().getString(R.string.wind),
                            weather.getWind(), weather.getGrade()));
                    break;
                default:
                    break;

            }
        }

    }
}
