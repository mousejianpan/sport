package zhaoyang.sport.fragment;


import android.animation.AnimatorSet;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.aspsine.swipetoloadlayout.OnLoadMoreListener;
import com.aspsine.swipetoloadlayout.OnRefreshListener;
import com.aspsine.swipetoloadlayout.SwipeToLoadLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import zhaoyang.sport.Bean.Happy;
import zhaoyang.sport.Bean.Moive;
import zhaoyang.sport.R;
import zhaoyang.sport.adapter.HappyAdapter;
import zhaoyang.sport.adapter.MoiveAdapter;
import zhaoyang.sport.presenter.HappyPresenter;
import zhaoyang.sport.presenter.MoivePresenter;
import zhaoyang.sport.view.MySwipeFooter;
import zhaoyang.sport.view.MySwipeHeader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HappyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HappyFragment extends Fragment implements IHappyView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ArrayList<Happy> happyArrayList;
    private RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.swipe_target)
    public RecyclerView recyclerView;
    @BindView(R.id.happy_progress)
    public ProgressBar pbProgrssbar;
    private HappyAdapter happyAdapter;
    private Unbinder unbinder;
    private HappyPresenter happyPresenter;
    private SwipeToLoadLayout swipeToLoadLayout;
    @BindView(R.id.swipe_refresh_header)
    public MySwipeHeader mySwipeHeader;
    @BindView(R.id.swipe_load_more_footer)
    public MySwipeFooter mySwipeFooter;
    private int start = 0;
    private boolean update = false;
    private boolean noData = false;
    private AnimatorSet animatorSet;
    private int page;


    public HappyFragment() {
        // Required empty public constructor
        happyPresenter = new HappyPresenter(this);
        happyArrayList = new ArrayList<>();
        page = 1;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HappyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HappyFragment newInstance(String param1, String param2) {
        HappyFragment fragment = new HappyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        happyPresenter.loadHappyNetData(1, page);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_happy, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public void initRecycleView() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        happyAdapter = new HappyAdapter(getActivity(), happyArrayList);
        recyclerView.setAdapter(happyAdapter);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = 5;
                outRect.right = 5;
                //outRect.bottom = 10;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = 20;
                //}
            }
        });
        swipeToLoadLayout = getActivity().findViewById(R.id.swipeToLoadLayout);
        if (swipeToLoadLayout != null) {
            // down load more
            swipeToLoadLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    update = true;
                    page += 1;
                    Log.e("david", "load more data");
                    happyPresenter.loadHappyNetData(1, 1);
                }
            });
            // up refresh
            swipeToLoadLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh() {
                    update = true;
                    page += 1;
                    Log.e("david", "refresh data");
                    happyPresenter.loadHappyNetData(1, 1);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void updateHappyData(ArrayList<Happy> arrayList) {
        pbProgrssbar.setVisibility(View.GONE);
        happyArrayList = arrayList;
        happyAdapter.updateData(happyArrayList);
        happyAdapter.notifyDataSetChanged();
        Log.e("david", "update happy data");
        if(swipeToLoadLayout.isLoadingMore()) {
            swipeToLoadLayout.setLoadingMore(false);
        }
        if (swipeToLoadLayout.isRefreshing()) {
            swipeToLoadLayout.setRefreshing(false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecycleView();
    }
}
