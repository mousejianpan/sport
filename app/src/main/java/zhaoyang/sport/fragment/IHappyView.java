package zhaoyang.sport.fragment;

import java.util.ArrayList;

import zhaoyang.sport.Bean.Happy;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public interface IHappyView {
    void updateHappyData(ArrayList<Happy> arrayList);
}
