package zhaoyang.sport.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import zhaoyang.sport.Bean.Moive;
import zhaoyang.sport.R;
import zhaoyang.sport.activity.CinemaActivity;
import zhaoyang.sport.adapter.MoiveAdapter;
import zhaoyang.sport.presenter.MoivePresenter;
import zhaoyang.sport.utils.MoiveNetUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoiveFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MoiveFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoiveFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private MoivePresenter moivePresenter;
    private ArrayList<Moive> moiveArrayList;
    private RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.moive_recycleview)
    public RecyclerView recyclerView;
    @BindView(R.id.moive_progress)
    public ProgressBar pbProgressBar;
    private MoiveAdapter moiveAdapter;
    private Unbinder unbinder;

    public MoiveFragment() {
        // Required empty public constructor
        moivePresenter = new MoivePresenter(getActivity());
        moiveArrayList = new ArrayList<>();
        moiveArrayList = moivePresenter.loadMoiveData();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoiveFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MoiveFragment newInstance(String param1, String param2) {
        MoiveFragment fragment = new MoiveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_moive, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("david", "onactivitycreate");
        initRecycleView();
    }

    public void initRecycleView() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        moiveAdapter = new MoiveAdapter(getActivity(), moiveArrayList);
        recyclerView.setAdapter(moiveAdapter);
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.left = 5;
                outRect.right = 5;
                //outRect.bottom = 10;
                //if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = 20;
                //}
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Subscribe(sticky = true)
    public void OnEvent(ArrayList<Moive> moiveArrayList) {
        Log.e("david", "receive moive" + Thread.currentThread().toString());
        if (moiveArrayList.size() > 0) {
            if (moiveArrayList.get(0) instanceof Moive) {
                pbProgressBar.setVisibility(View.GONE);
                moiveAdapter.updateData(moiveArrayList);
                moiveAdapter.notifyDataSetChanged();
            }
        }
    }
}
