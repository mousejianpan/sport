package zhaoyang.sport.Bean;

import java.io.Serializable;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-7.
 */

public class Cinema implements Serializable {
    private String name;
    private String addr;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}
