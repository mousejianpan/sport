package zhaoyang.sport.Bean;

import java.io.Serializable;

/**
 * Created by SPREADTRUM\david.zhao on 18-5-9.
 */

public class Happy implements Serializable {
    private int type;
    private String text;
    private String videoUrl;
    private String pictureUrl;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
